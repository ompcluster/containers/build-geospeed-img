#!/usr/bin/env python

from __future__ import print_function
from functools import partial

import re
import argparse
import hpccm
import os
import itertools
from hpccm.building_blocks import *
from hpccm.primitives import *


def generateRecipeAwave(baseImage):
    Stage0 = hpccm.Stage()

    Stage0 += baseimage(image=baseImage)

    # Install git-lfs
    if 'centos' in baseImage:
        Stage0 += shell(commands=['curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | sudo bash'])
    Stage0 += packages(ospackages=['git-lfs'], epel=True)
    Stage0 += shell(commands=['git lfs install'])

    # Install Awave/SeisUnix/Madagascar dependencies and useful tools
    Stage0 += packages(apt=['xauth', 'build-essential', 'libx11-dev',
                            'libxt-dev', 'freeglut3', 'freeglut3-dev',
                            'libxmu-dev', 'libxi-dev',
                            'apt-utils', 'less', 'ghostscript', 'man-db',
                            'manpages-posix',
                            'libglib2.0-dev', 'python'],
                       yum=['glib2-devel', 'python2'], epel=True)

    Stage0 += pip(pip='pip3', packages=['numpy'])

    # Install SeisUnix to visualize datasets
    Stage0 += environment(variables={'CWPROOT': '/opt/SeisUnix',
                                     'PATH': '/opt/SeisUnix/bin:$PATH'})
    Stage0 += generic_build(build=['mkdir $CWPROOT', 'cp -R ./* $CWPROOT',
                                   'cd $CWPROOT/src',
                                   'make install', 'make xtinstall',
                                   'rm -rf $CWPROOT/src'],
                            repository='https://github.com/jjssobrinho/SeisUnix')

    # Install Madagascar (dit not work with git repository)
    Stage0 += generic_autotools(prefix='/opt/madagascar',
                                url='https://sourceforge.net/projects/rsf/files/madagascar/madagascar-3.0/madagascar-3.0.1.tar.gz',
                                directory='madagascar-3.0',
                                preconfigure=['rm -rf ./user/*'],
                                devel_environment={'RSF_ROOT': '/opt/madagascar'})

    hpccm.config.set_container_format('docker')

    return Stage0


def combine(list1, list2):
    return map(lambda x: '-'.join(x), list(itertools.product(list1, list2)))


def main():
    print("Generating App image definitions...")

    # list of combinations of configurations that can be used
    allConf = ["ubuntu18.04", "ubuntu20.04", "centos7"]
    allConf += combine(allConf, ["cuda10.1", "cuda10.2", "cuda11.2"])
    allConf = combine(allConf, ["mpich", "mpich-legacy",
                                "openmpi", "openmpi-legacy",
                                "mvapich2"])

    outputFolder = "Dockerfiles"

    for currConf in allConf:
        baseImageDev = "ompcluster/runtime:" + currConf
        baseImageExp = "ompcluster/runtime-dev:" + currConf

        # Generation of Dockerfile for Awave
        defFileTextAwaveDev = str(generateRecipeAwave(baseImageDev))
        defFileTextAwaveExp = str(generateRecipeAwave(baseImageExp))
        defFileNameAwaveDev = "awave-dev-" + currConf
        defFileNameAwaveExp = "awave-exp-" + currConf

        # save container definition file
        with open(f"{outputFolder}/{defFileNameAwaveDev}", "w") as text_file:
            text_file.write(str(defFileTextAwaveDev))
        with open(f"{outputFolder}/{defFileNameAwaveExp}", "w") as text_file:
            text_file.write(str(defFileTextAwaveExp))


if __name__ == "__main__":
    main()
